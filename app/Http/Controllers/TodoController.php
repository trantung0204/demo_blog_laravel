<?php

namespace App\Http\Controllers;
use App\Todo; //sử dụng Class Todo trong model Todo.php

use Illuminate\Http\Request;

class TodoController extends Controller
{
    public function getAll()
    {
    	$todos=Todo::all(); //phương all() được Laravel định nghĩa sẵn, trả về dữ liệu tất cả các bản ghi trong bảng todos.
    	return view('todo',compact('todos')); //trả dữ liệu trong biết $todos sang cho file view todo.blade.php sẽ được đề cập ngay sau đây.
    }
    public function add()
    {
    	return view('add');
    }
    public function edit($id)
    {
    	$todo=Todo::find($id);
    	return view('edit',compact('todo'));
    }
}
